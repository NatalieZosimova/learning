package controls;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class BaseElement {
    private WebDriver driver;
    private By locator;

    public BaseElement(WebDriver driver, By locator) {
        this.driver = driver;
        this.locator = locator;
    }

    public WebElement getElement() {
        return driver.findElement(locator);
    }

    public String getAttribute(String name) {
        return getElement().getAttribute(name);
    }

}
