package controls;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.Select;

public class DropDown extends BaseElement implements IClickable, ISetable {

    public DropDown(WebDriver driver, By locator) {
        super(driver, locator);
    }


    @Override
    public void click() {
        getElement().click();
    }

    @Override
    public void setText(String text) {
        Select drpDwn = new Select(getElement());
        drpDwn.selectByVisibleText(text);

    }
}
