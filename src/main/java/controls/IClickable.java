package controls;

public interface IClickable {
    void click();
}
