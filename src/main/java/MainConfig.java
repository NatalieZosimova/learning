import org.aeonbits.owner.Config;

@Config.Sources("file:./src/main/resources/MainConfig.properties")
public interface MainConfig extends Config {
    String swaggerURL();
    String aqaURL();

}