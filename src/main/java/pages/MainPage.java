package pages;

import controls.ClickableElement;
import controls.Input;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;

public class MainPage extends BasePage {

    public MainPage(WebDriver driver) {
        super(driver);
    }

    public LoginPage clickSignInLink() {
        new ClickableElement(getDriver(), By.xpath("//a[contains(text(),'Sign in')]")).click();
        LoginPage login = new LoginPage(getDriver());
        login.getWait().until(ExpectedConditions.visibilityOfElementLocated(By.id("email_create")));
        return login;
    }

}
