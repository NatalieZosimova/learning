import dto.TestUser;
import helpers.UserDecorator;
import org.testng.Assert;
import org.testng.annotations.Test;
import pages.LoginPage;
import pages.MainPage;
import services.UserService;

public class ShopTest extends BaseTest{

    @Test
    public void goToShop()  {
        TestUser testUser = new UserDecorator().fillData(new TestUser());

        UserService.createUser(testUser);

        TestUser siteUser = UserService.getUserByName(testUser.getUsername());

        Assert.assertEquals(testUser, siteUser);

        // add data
        siteUser = new UserDecorator().fillData(siteUser);
        // go to shop and create user account there
        getDriver().get(getAqaURL());
        MainPage main = new MainPage(getDriver());
        LoginPage login = main.clickSignInLink()
                .inputEmailToCreateAccount(siteUser.getEmail())
                .fillRegisterForm(siteUser);



       Assert.assertEquals(login.getWelcomeLabel().getText(),
                "Welcome to your account. Here you can manage all of your personal information and orders.");

    }

}
