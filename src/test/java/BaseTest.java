import io.github.bonigarcia.wdm.WebDriverManager;
import io.restassured.RestAssured;
import org.aeonbits.owner.ConfigFactory;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.BeforeTest;


public class BaseTest {

    private WebDriver driver;
    private String swaggerURL;
    private String aqaURL;

    public WebDriver getDriver() {
        return driver;
    }

    public String getSwaggerURL() {
        return swaggerURL;
    }

    public String getAqaURL() {
        return aqaURL;
    }

    final Logger logger = LoggerFactory.getLogger(BaseTest.class);



    @BeforeSuite
    public void init() {
        WebDriverManager.chromedriver().setup();
        initConfig();
        RestAssured.baseURI = getSwaggerURL();
        RestAssured.basePath = "/v2";
    }

    @BeforeTest
    public void setupTest() {
        ChromeOptions options = new ChromeOptions();
        options.addArguments("start-maximized");
        driver = new ChromeDriver(options);
    }


    @AfterTest
    public void afterSuite() {
        if (driver != null) {
            driver.quit();
        }
    }

    private void initConfig() {
            MainConfig cfg = ConfigFactory.create(MainConfig.class);
            this.swaggerURL = cfg.swaggerURL();
            this.aqaURL = cfg.aqaURL();
    }



}

